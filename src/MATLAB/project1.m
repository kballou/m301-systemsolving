function project1()
    A = [ 5 -3 -7 1; 0 11 9 5; 0 0 3 -13; 0 0 0 7 ];
    b = [ -14; 22; -11; 14 ];
    C = [ 4 -1 2 2 -1; 0 -2 6 2 7; 0 0 1 -1 -2; 0 0 0 -2 -1; 0 0 0 0 3 ];
    d = [ 4; 0; 3; 10; 6 ];
    tic;
    x = backsub(A, b);
    problem1_compute_time = toc;
    tic;
    y = backsub(C, d);
    problem2_compute_time = toc;
    fprintf('First Problem Solution:\n%s\n', sprintf('   %f\n', x));
    fprintf('\tCompute Time: %f sec\n', problem1_compute_time);
    fprintf('Second Problem Solution:\n%s\n', sprintf('   %f\n', y));
    fprintf('\tCompute Time: %f sec\n', problem2_compute_time);
end
