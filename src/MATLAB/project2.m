function project2()
    A = [ 2 4 -6; 1 5 3; 1 3 2 ];
    b = [ -4; 10; 5 ];
    C = [ 4 8 4 0; 1 5 4 -3; 1 4 7 2; 1 3 0 -2 ];
    d = [ 8; -4; 10; -4 ];
    tic;
    [A, b] = gaussian(A, b);
    x = backsub(A, b);
    problem1_compute_time = toc;
    [A_expected, A_l] = lu(A);
    tic;
    [C, d] = gaussian(C, d);
    y = backsub(C, d);
    problem2_compute_time = toc;
    fprintf('First Problem Solution:\n%s\n', sprintf('   %f\n', x))
    fprintf('\tCompute Time: %f sec\n', problem1_compute_time);
    fprintf('Second Problem Solution:\n%s\n', sprintf('   %f\n', y))
    fprintf('\tCompute Time: %f sec\n', problem2_compute_time);
end
