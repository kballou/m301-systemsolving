function A, b = gaussian(A, b)
    assert(size(A)(1) == size(A)(end), 'Matrix is not square!');
    assert(size(A)(1) == size(b)(1), 'A is not compatible with vector b');
    assert(det(A) != 0, 'det(A) is 0!');
    M = [A b];
    for i = 1:length(A)
        for j = i+1:length(A)
            M(j,:) = M(j,:) - (M(j,i)/M(i,i)) * M(i,:);
        end
    end
    A = M(:,1:length(A));
    b = M(:,end);
end
