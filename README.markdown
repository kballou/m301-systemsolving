# Math 301: Solving Systems #

## Project 1 ##

Write a MATLAB function that solves any system `Ax=b`, where `A` is an
upper-triangular `n` by `n` matrix such that `det(A) ≠ 0`, `n` is any
positive integer, and `b∈\R^n` . Use your function to solve the systems:

    5x_1 - 3x_2 - 7x_3 +   x_4 = -14
          11x_2 + 9x_3 +  5x_4 =  22
                  3x_3 - 13x_4 = -11
                          7x_4 =  14

    4x_1 -  x_2 + 2x_3 +  2x_4 -  x_5 =   4
         - 2x_2 + 6x_3 +  2x_4 + 7x_5 =   0
                   x_3 -   x_4 - 2x_5 =   3
                       -  2x_4 -  x_5 =  10
                                 3x_5 =   6

## Project 2 ##

Write a MATLAB a function that uses Gaussian eliminiation and solves any system
`Ax = b`, where `A∈\R^{nxn}` is such that `det(A) ≠ 0`, `b∈\R^n`, and `n` is
any positive integer. The function is supposed to reduce `[ A | b ]` to an
upper-triangular matrix and use back substitution to determine `x∈\R^n`. Use
your function to solve the systems:

    2x_1 + 4x_2 - 6x_3 =  -4
     x_1 + 5x_2 + 3x_3 =  10
     x_1 + 3x_2 + 2x_3 =   5

    4x_1 + 8x_2 + 4x_3        =   8
     x_1 + 5x_2 + 4x_3 - 3x_4 =  -4
     x_1 + 4x_2 + 7x_3 + 2x_4 =  10
     x_1 + 3x_2        - 2x_4 =  -4
